
const variants = {
  primary: {
    base: "bg-orange-gradient-vertical text-white",
    hover: "bg-orange-gradient-vertical-500"
  },
  fancy: {
    base: "bg-orange-gradient text-white font-light",
    hover: "bg-orange-gradient-500"
  }
}

export default variants;